<html>
    <body>
        <h4>bitwise AND operator</h4>
        
        <?php
            $x = 5;
            $y = 6;
            $c = $x & $y;
            
            echo $c;
        ?>
        
        <br><br>
        
        <h4>bitwise OR operator</h4>
        <?php
            $x = 5;
            $y = 6;
            $c = $x | $y;
            
            echo $c;
        ?>
        
               <br><br>
        
        <h4>bitwise XOR operator</h4>
        <?php
            $x = 5;
            $y = 6;
            $c = $x ^ $y;
            
            echo $c;
        ?>
        
    </body>
    
</html>
