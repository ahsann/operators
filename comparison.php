<?php
echo "<pre>";
$x = 100;
$y = "100";
var_dump($x == $y); // returns true because values are equal
var_dump($x != $y); // returns false because values are equal
var_dump($x === $y); // returns false because values are not equal
var_dump($x !== $y); // returns true because values are not equal
var_dump($x >= $y); // returns true because values are equal


// Bool and null are compared as bool always
var_dump(1 == TRUE);  // TRUE - same as (bool)1 == TRUE
var_dump(0 == FALSE); // TRUE - same as (bool)0 == FALSE
var_dump(100 < TRUE); // FALSE - same as (bool)100 < TRUE
var_dump(-10 < FALSE);// FALSE - same as (bool)-10 < FALSE

echo "</pre>";
?>  
