<?php
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
     echo "Hello world!";//if $x or $y is true not both.
}
echo "<br><br>";
?>   

<?php
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
     echo "Hello world!";//if both are true
}
echo "<br><br>";
?>   

<?php
$x = 100;
$y = 50;

if ($x == 100 or $y == 60) {
     echo "Hello world!";//anyone is true.no problem if both are true.
}
echo "<br><br>";
?>   

<?php
$x = 100;
$y = 50;

if ($x == 100 && $y == 50) {
     echo "Hello world!";// symbolic form of and operator &&.
}
echo "<br><br>";
?> 

<?php
$x = 100;
$y = 50;

if ($x == 100 || $y == 50) {
     echo "Hello world!";// symbolic form of or operator ||.
}
echo "<br><br>";
?> 

<?php
$x = 100;

if ($x !== 90) {
     echo "Hello world!";//not operator.    
}
echo "<br><br>";
?>   

<?php
// "&&" has a greater precedence than "and";
// "||" has a greater precedence than "or"  
?>
